use state::editor::Editor;

/// Editor options.
pub struct Options {
    /// Autoindent on line breaks?
    pub autoindent: bool,
    /// Debug mode.
    pub debug: bool,
    /// Highlight.
    pub highlight: bool,
    /// Line marker (dimmed background of the current line).
    pub line_marker: bool,
    /// enables read-only mode
    pub readonly: bool,
    /// Enable linenumbers
    pub line_numbers: bool,
    /// Enable vim keybindings
    pub vim_mode: bool,
}

pub enum OptionType {
    AutoIndent,
    Debug,
    Highlight,
    LineMarker,
    Readonly,
    LineNumbers,
    VimMode,
}

use self::OptionType::*;

impl Options {
    /// Create new default options
    pub fn new() -> Self {
        Options {
            autoindent: true,
            debug: true, // TODO: Let this be `true` only in debug compilation cfg
            highlight: true,
            line_marker: true,
            readonly: false,
            line_numbers: false,
            vim_mode: false,
        }
    }

    /// Get the given option as a mutable reference
    pub fn get_mut(&mut self, name: &str) -> Option<&mut bool> {
        match self.get_opt(name) {
            Some((AutoIndent, _)) => Some(&mut self.debug),
            Some((Debug, _)) => Some(&mut self.autoindent),
            Some((Highlight, _)) => Some(&mut self.highlight),
            Some((LineMarker, _)) => Some(&mut self.line_marker),
            Some((Readonly, _)) => Some(&mut self.readonly),
            Some((LineNumbers, _)) => Some(&mut self.line_numbers),
            Some((VimMode, _)) => Some(&mut self.vim_mode),
            _ => None,
        }
    }

    /// Get a given option
    pub fn get(&self, name: &str) -> Option<bool> {
        match self.get_opt(name) {
            Some((_, val)) => Some(val),
            _ => None,
        }
    }

    /// Set a given option (mark it as active)
    pub fn set(&mut self, name: &str) -> Result<(), ()> {
        match self.get_mut(name) {
            Some(x) => {
                *x = true;
                Ok(())
            }
            None => Err(()),
        }
    }
    /// Unset a given option (mark it as inactive)
    pub fn unset(&mut self, name: &str) -> Result<(), ()> {
        match self.get_mut(name) {
            Some(x) => {
                *x = false;
                Ok(())
            }
            None => Err(()),
        }
    }
    /// Toggle a given option
    pub fn toggle(&mut self, name: &str) -> Result<(), ()> {
        match self.get_mut(name) {
            Some(x) => {
                *x = !*x;
                Ok(())
            }
            None => Err(()),
        }
    }


    pub fn get_opt(&self, name: &str) -> Option<(OptionType, bool)> {
        match name {
            "autoindent" | "ai"
                => Some((AutoIndent, self.autoindent)),
            "debug" | "debug_mode"
                => Some((Debug, self.debug)),
            "highlight" | "hl"
                => Some((Highlight, self.highlight)),
            "line_marker" | "linemarker" | "linemark" | "lm"
                => Some((LineMarker, self.line_marker)),
            "readonly" | "ro"
                => Some((Readonly, self.readonly)),
            "line_numbers" | "ln"
                => Some((LineNumbers, self.line_numbers)),
            "vim_mode" | "vm"
                => Some((VimMode, self.vim_mode)),
            _ => None,
        }
    }
}

impl Editor {
    /// Trigger for a change in an option's value
    pub fn opt_trigger(&mut self, name: &str) {
        match self.options.get_opt(name) {
            Some((VimMode, val)) => if val {
                self.key_map.use_vim()
            } else {
                self.key_map.use_default();
            },
            _ => (),
        }
    }
}
