use caret::position::to_signed_pos;
use edit::buffer::TextBuffer;
use io::parse::Inst;
use io::key_map::Action;
use state::editor::Editor;

impl Editor {
    /// Convert an instruction to a motion (new coordinate). Returns None if the instructions given
    /// either is invalid or has no movement.
    ///
    /// A motion is a namespace (i.e. non mode-specific set of commands), which represents
    /// movements. These are useful for commands which takes a motion as post-parameter, such as d.
    /// d deletes the text given by the motion following. Other commands can make use of motions,
    /// using this method.
    pub fn to_motion(&mut self, Inst(n, cmd): Inst) -> Option<(usize, usize)> {
        let y = self.y();

        match self.key_map.reverse_lookup(cmd.key) {
            Action::MoveLeft => Some(self.left(n.d())),
            Action::MoveRight => Some(self.right(n.d(), true)),
            Action::MoveDown => Some(self.down(n.d())),
            Action::MoveUp => Some(self.up(n.d())),
            Action::GoTo => Some((0, n.or(1) - 1)),
            Action::GoToEnd => Some((0, self.buffers.current_buffer().len() - 1)),
            Action::EndOfLine => Some((self.buffers.current_buffer()[y].len() - 1, y)),
            Action::StartOfLine => Some((0, y)),
            Action::NextOccur => {
                let ch = self.get_char();

                if let Some(o) = self.next_ocur(ch, n.d()) {
                    Some((o, y))
                } else {
                    None
                }
            }
            Action::PrevOccur => {
                let ch = self.get_char();

                if let Some(o) = self.previous_ocur(ch, n.d()) {
                    Some((o, y))
                } else {
                    None
                }
            }
            Action::Unrecognized(c) => {
                self.status_bar.msg = format!("Motion not defined: '{}'", c);
                self.redraw_status_bar();
                None
            }
            _ => {
                self.status_bar.msg = format!("Motion not defined");
                None
            }
        }
    }
    /// Like to_motion() but does not bound to the text. Therefore it returns an isize, and in some
    /// cases it's a position which is out of bounds. This is useful when commands want to mesure
    /// the relative movement over the movement.
    pub fn to_motion_unbounded(&mut self, Inst(n, cmd): Inst) -> Option<(isize, isize)> {
        let y = self.y();
        match self.key_map.reverse_lookup(cmd.key) {
            Action::MoveLeft => Some(self.left_unbounded(n.d())),
            Action::MoveRight => Some(self.right_unbounded(n.d())),
            Action::MoveDown => Some(self.down_unbounded(n.d())),
            Action::MoveUp => Some(self.up_unbounded(n.d())),
            Action::GoTo => Some((0, n.or(1) as isize - 1)),
            Action::GoToEnd => Some((
                self.buffers.current_buffer()[y].len() as isize,
                self.buffers.current_buffer().len() as isize - 1,
            )),
            Action::EndOfLine => Some(to_signed_pos((self.buffers.current_buffer()[y].len(), y))),
            Action::StartOfLine => Some((0, y as isize)),
            Action::NextOccur => {
                let ch = self.get_char();

                if let Some(o) = self.next_ocur(ch, n.d()) {
                    Some(to_signed_pos((o, y)))
                } else {
                    None
                }
            }
            Action::PrevOccur => {
                let ch = self.get_char();

                if let Some(o) = self.previous_ocur(ch, n.d()) {
                    Some(to_signed_pos((o, y)))
                } else {
                    None
                }
            }
            _ => None,
        }
    }
}
