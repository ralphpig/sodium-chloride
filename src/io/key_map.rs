use std::collections::HashMap;
use io::key::Key;
use io::key::Key::*;

/// quick enum for selecting keybind set
#[derive(Eq, PartialEq)]
pub enum KeybindMode {
    /// for users accustomed to vim
    VimMode,
    /// sodium defaults
    Default,
}

/// Struct for holding custom key_map
pub struct KeyMap {
    key_map: HashMap<Action, Key>,
}

impl KeyMap {
    /// setup a new key_map
    pub fn new(mode: Option<KeybindMode>) -> KeyMap {
        let mut keys = KeyMap {
            key_map: HashMap::new(),
        };
        match mode {
            Some(m) => if m == KeybindMode::VimMode {
                keys.use_vim();
            } else {
                keys.use_default();
            }
            None => keys.use_default(),
        }
        keys
    }
    /// Add a new keybind for an action
    pub fn insert(&mut self, action: Action, key: Key) {
        self.key_map.remove_entry(&action);
        self.key_map.insert(action, key);
    }
    /// Return the key used for a given action
    pub fn key_for(&self, action: Action) -> Key {
        let res = self.key_map.get(&action);
        match res {
            Some(key) => *key,
            None => Key::Null,
        }
    }

    /// DICKS
    pub fn reverse_lookup(&self, input: Key) -> Action {
        match input {
            Left => return Action::MoveLeft,
            Right => return Action::MoveRight,
            Up => return Action::MoveUp,
            Down => return Action::MoveDown,
            _ => {},
        }
        for (key, val) in self.key_map.iter() {
            if *val == input {
                return *key;
            }
        }
        Action::Unrecognized(input.to_char())
    }
    /// add default sodium bindings to key_map
    pub fn use_default(&mut self) {
        self.key_map.clear();
        self.insert(Action::Escape,         Escape);
        self.insert(Action::Spacebar,       Char(' '));
        self.insert(Action::Backspace,      Backspace);
        self.insert(Action::Enter,          Char('\n'));
        self.insert(Action::Insert,         Char('i'));
        self.insert(Action::AltInsert,      Char('I'));   
        self.insert(Action::Append,         Char('a'));   
        self.insert(Action::AltAppend,      Char('A'));   
        self.insert(Action::MoveLeft,       Char('h'));   
        self.insert(Action::MoveRight,      Char('l'));   
        self.insert(Action::MoveUp,         Char('k'));
        self.insert(Action::AltMoveUp,      Char('K'));   
        self.insert(Action::MoveDown,       Char('j'));   
        self.insert(Action::AltMoveDown,    Char('J'));   
        self.insert(Action::Remove,         Char('x'));   
        self.insert(Action::AltRemove,      Char('X'));   
        self.insert(Action::StartOfLine,    Char('H'));   
        self.insert(Action::EndOfLine,      Char('L'));   
        self.insert(Action::Scroll,         Char('z'));   
        self.insert(Action::AltScroll,      Char('Z'));   
        self.insert(Action::Branch,         Char('b'));   
        self.insert(Action::UnBranch,       Char('B'));   
        self.insert(Action::Delete,         Char('d'));
        self.insert(Action::Change,         Char('c'));
        self.insert(Action::Replace,        Char('r'));   
        self.insert(Action::ReplaceMode,    Char('R'));   
        self.insert(Action::NewLine,        Char('o'));   
        self.insert(Action::CounterPart,    Char('~'));   
        self.insert(Action::NextWord,       Char('W'));
        self.insert(Action::GoTo,           Char('g'));
        self.insert(Action::GoToEnd,        Char('G'));
        self.insert(Action::NextOccur,      Char('t'));
        self.insert(Action::PrevOccur,      Char('f'));
        self.insert(Action::Yank,           Char('y'));   
        self.insert(Action::Put,            Char('p'));   
        self.insert(Action::RepeatLast,     Char('.'));   
        self.insert(Action::PromptMode,     Char(';'));   
    }
    /// use vim hotkeys instead
    pub fn use_vim(&mut self) {
        self.key_map.clear();
        self.insert(Action::Escape,         Escape);
        self.insert(Action::Spacebar,       Char(' '));
        self.insert(Action::Backspace,      Backspace);
        self.insert(Action::Enter,          Char('\n'));
        self.insert(Action::Insert,         Char('i'));
        self.insert(Action::AltInsert,      Char('I'));   
        self.insert(Action::Append,         Char('a'));   
        self.insert(Action::AltAppend,      Char('A'));   
        self.insert(Action::MoveLeft,       Char('h'));   
        self.insert(Action::MoveRight,      Char('l'));   
        self.insert(Action::MoveUp,         Char('k'));
        self.insert(Action::AltMoveUp,      Char('U'));   
        self.insert(Action::MoveDown,       Char('j'));   
        self.insert(Action::AltMoveDown,    Char('D'));   
        self.insert(Action::Remove,         Char('x'));   
        self.insert(Action::AltRemove,      Char('X'));   
        self.insert(Action::StartOfLine,    Char('0'));   
        self.insert(Action::EndOfLine,      Char('$'));   
        self.insert(Action::Scroll,         Char('z'));   
        self.insert(Action::AltScroll,      Char('Z'));   
        self.insert(Action::Branch,         Char('b'));   
        self.insert(Action::UnBranch,       Char('B'));   
        self.insert(Action::Delete,         Char('d'));
        self.insert(Action::Change,         Char('c'));
        self.insert(Action::Replace,        Char('r'));   
        self.insert(Action::ReplaceMode,    Char('R'));   
        self.insert(Action::NewLine,        Char('o'));   
        self.insert(Action::CounterPart,    Char('~'));   
        self.insert(Action::NextWord,       Char('w'));
        self.insert(Action::GoTo,           Char('g'));
        self.insert(Action::GoToEnd,        Char('G'));
        self.insert(Action::NextOccur,      Char('t'));
        self.insert(Action::PrevOccur,      Char('f'));
        self.insert(Action::Yank,           Char('y'));   
        self.insert(Action::Put,            Char('p'));   
        self.insert(Action::RepeatLast,     Char('.'));   
        self.insert(Action::PromptMode,     Char(':')); 
    }
}

/// Universal set of actions
#[derive(Copy, Clone, Eq, PartialEq, Hash)]
pub enum Action {
    Unrecognized(char), 
    Escape,             // esc
    Spacebar,           // space
    Backspace,          // backspace
    Enter,              // \n
    Insert,             // i   
    AltInsert,          // I
    Append,             // a
    AltAppend,          // A
    MoveLeft,           // h
    MoveRight,          // l
    MoveUp,             // k
    AltMoveUp,          // K
    MoveDown,           // j
    AltMoveDown,        // J
    Remove,             // x
    AltRemove,          // X
    StartOfLine,        // H
    EndOfLine,          // L
    Scroll,             // z
    AltScroll,          // Z
    Branch,             // b
    UnBranch,           // B
    Delete,             // d
    Change,             // c
    Replace,            // r
    ReplaceMode,        // R
    NewLine,            // o
    CounterPart,        // ~
    NextWord,           // W
    GoTo,               // g
    GoToEnd,            // G
    NextOccur,          // t
    PrevOccur,          // f
    Yank,               // y
    Put,                // p
    RepeatLast,         // .
    PromptMode,         // ;
}